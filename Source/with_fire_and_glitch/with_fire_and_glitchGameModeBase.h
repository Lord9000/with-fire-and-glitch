// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "with_fire_and_glitchGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class WITH_FIRE_AND_GLITCH_API Awith_fire_and_glitchGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
