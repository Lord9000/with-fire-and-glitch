// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ParticleDefinitions.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WITH_FIRE_AND_GLITCH_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int maxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int currentHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UParticleSystem* DamageFX;



public:
	UFUNCTION(BlueprintCallable)
	void Damage(int hp);

	UFUNCTION(BlueprintCallable)
	void Heal(int hp);

	UFUNCTION(BlueprintCallable)
	int GetMaxHealth();

	UFUNCTION(BlueprintCallable)
	int GetCurrentHealth();

	// Sets default values for this component's properties
	UHealthComponent();


	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};
