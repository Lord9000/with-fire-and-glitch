// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickup.h"
#include "HealthPickup.generated.h"

/**
 * 
 */
UCLASS()
class WITH_FIRE_AND_GLITCH_API AHealthPickup : public ABasePickup
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		int healAmount;

	virtual void PickupAction(AFPSCharacter* character) override;
	
};
