// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Glitch.h"

/**
 * 
 */
class WITH_FIRE_AND_GLITCH_API ColliderGlitch : public Glitch
{
public:
	ColliderGlitch();
	virtual ~ColliderGlitch();

	virtual void ApplyGlitch(UActorComponent* comp) override;
	virtual void ApplyGlitch(UActorComponent* hitComp, UActorComponent* proComponent) override {}
};
