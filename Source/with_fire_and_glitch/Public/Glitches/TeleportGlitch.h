// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "TimedGlitch.h"

#include "CoreMinimal.h"

class ACharacter;

/**
 * 
 */
class WITH_FIRE_AND_GLITCH_API TeleportGlitch : public TimedGlitch
{
public:
	TeleportGlitch();
	TeleportGlitch(float duration) : TimedGlitch(duration) {}
	~TeleportGlitch();
	virtual void ApplyGlitch(UActorComponent* hitComp, UActorComponent* proComponent);
	virtual void ApplyGlitch(UActorComponent* comp) {}
	virtual void OnGlitchTick(UActorComponent* comp) override;
	virtual void DiscardGlitch(UActorComponent* comp) override;

private:
	float tickDistance;
	FVector teleportMarker;
	bool teleport;
};
