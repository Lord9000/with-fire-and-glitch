// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class UActorComponent;

/**
 * 
 */
class WITH_FIRE_AND_GLITCH_API Glitch
{
public:
	virtual void ApplyGlitch(UActorComponent* comp) = 0;
	/// <summary>
	/// 
	/// </summary>
	/// <param name="hitComp">Hitted component</param>
	/// <param name="proComponent">Projectile component</param>
	virtual void ApplyGlitch(UActorComponent* hitComp, UActorComponent* proComponent) = 0;
	virtual ~Glitch () {}
};
