// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Glitch.h"
#include <vector>


struct GlitchedObject
{
	UActorComponent* comp;
	float time;

	GlitchedObject(UActorComponent* c, float t)
	{
		comp = c;
		time = t;
	}
};

/**
 * 
 */
class WITH_FIRE_AND_GLITCH_API TimedGlitch : public Glitch
{
public:
	TimedGlitch();
	TimedGlitch(float duration);
	virtual ~TimedGlitch();

	virtual void ApplyGlitch(UActorComponent* comp) override;
	virtual void ApplyGlitch(UActorComponent* hitComp, UActorComponent* proComponent) override;
	virtual void OnGlitchTick(UActorComponent* comp) = 0;
	virtual void DiscardGlitch(UActorComponent* comp);

	std::vector<GlitchedObject>* GetGlitchedObjects() { return &objects; }

protected:
	std::vector<GlitchedObject> objects;
	float duration;
};
