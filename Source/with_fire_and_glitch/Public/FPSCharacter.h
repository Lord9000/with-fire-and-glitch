// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#define MAX_GLITCHES 6

#include "Weapons/GrenadeLauncher.h"
#include "Weapons/Katarynka.h"
#include "Weapons/Pistol.h"
#include "Weapons/BasicGun.h"
#include "Engine.h"
#include "ParticleDefinitions.h"
#include "GameFramework/Character.h"
#include "GenericTeamAgentInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Components/PawnNoiseEmitterComponent.h"


// MUST BE LAST INCLUDE BECAUSE OF UNREAL
#include "FPSCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class UCameraComponent;
class AFPSProjectile;
class USoundBase;
class UAnimSequence;
class Glitch;

UENUM(BlueprintType)
enum class EGlitchType : uint8
{
	E_Destroy UMETA(DisplayName = "DestroyGlitch"),
	E_Spawn UMETA(DisplayName = "SpawnGlitch"),
	E_Teleport UMETA(DisplayName = "TeleportGlitch"),
};

UCLASS()
class AFPSCharacter : public ACharacter, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

protected:

	/** Pawn mesh: 1st person view  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
		USkeletalMeshComponent* Mesh1PComponent;	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Noise")
		UPawnNoiseEmitterComponent* PawnNoiseEmitter;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
		UCameraComponent* CameraComponent;

	// IGenericTeamAgentInterface implemetation
	virtual void SetGenericTeamId(const FGenericTeamId& TeamID) override;
	virtual FGenericTeamId GetGenericTeamId() const override;

public:
	AFPSCharacter();


	///** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		USkeletalMeshComponent* GunMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		TArray<USkeletalMesh*> GunMeshes;

	/** Array of weapons */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		TArray<ABasicGun*> GunsCollection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		int gunIndex;

	
	/** Projectile class to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		TSubclassOf<AFPSProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
		USoundBase* FireSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		UParticleSystem* SmokeFX;
	/** AnimMontage to play each time we fire */
	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
		UAnimSequence* FireAnimation;

	UPROPERTY(EditDefaultsOnly, Category = "Noise")
		float shootNoise;

	UFUNCTION(BlueprintImplementableEvent)
	void ShotsFired();

	UFUNCTION(BlueprintImplementableEvent)
		void OnWeaponChange();
	UFUNCTION(BlueprintImplementableEvent)
		void ActivateGlitchPostprocess();
	UFUNCTION(BlueprintImplementableEvent)
		void DeActivateGlitchPostprocess();

	UFUNCTION(BlueprintImplementableEvent)
		void DamagePostprocess();
	UFUNCTION(BlueprintImplementableEvent)
		void HealPostprocess();

	void HealPostprocessInvoke();
	void DamagePostprocessInvoke();
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		int GetAmmoByWeaponIndex(int index);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
		bool IsNextBullteGlitched(int index);


	/** The total ammount of ammo being carried for the weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		int grenadeAmmo;

	virtual void Tick(float DeltaTime) override;

	virtual void Landed(const FHitResult& Hit) override;


	/** The total ammount of ammo being carried for the weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		int pistolAmmo;

	/** The total ammount of ammo being carried for the weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		int machineGunAmmo;

	/** Checks if player is zoomingin */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool isZoomedIn;

	/** Checks if player is firing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool isShooting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool isGlitched;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool outofAmmo;

	/** The fire timer handle */
		FTimerHandle fireTimerHandle;
	/** The reload timer handle */
		FTimerHandle reloadTimerHandle;

		Glitch* nextGlitchesList[MAX_GLITCHES];

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Glitch")
		TArray<EGlitchType> UnrealNextGlitchesList;
		
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Glitch")
		int glitchIterator;

	

protected:
	virtual void BeginPlay() override;

	/** Fires a projectile. */
	void Fire();

	/** Fires a Raycast. */
	void FireaRaycast();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles strafing movement, left and right */
	void MoveRight(float Val);

	void ZoomIn();

	void StopZoom();
	
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

public:

	void AddAmmo(EWeaponType _weaponType, int _ammoAmount);
	
	void SwitchToNextPrimaryWeapon();

	void SwitchToPreviousPrimaryWeapon();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void StartFirirng();
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void StartFirirngAfterOverload();
	void StopFirirng();

	void StopFirirngWithoutMeshUpdate();

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
	void SwitchWeaponMesh(int _index);

	UFUNCTION(BlueprintImplementableEvent, Category = "HUD")
		void TriggerOutOfAmmmoPopUp();

	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1PComponent; }

	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return CameraComponent; }

private:

	void CheckJump();

	UPROPERTY()
		bool jumping;
	UPROPERTY()
		int jumpCount;

	FGenericTeamId teamID;
};

