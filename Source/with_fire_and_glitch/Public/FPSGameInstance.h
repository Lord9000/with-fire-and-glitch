// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define GTIMERTICK 0.1

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "AssetRegistry/AssetRegistryModule.h"
#include <vector>
#include "FPSGameInstance.generated.h"

class Glitch;
class TimedGlitch;
class AGlitchTimer;

/**
 * 
 */
UCLASS()
class WITH_FIRE_AND_GLITCH_API UFPSGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;
	virtual void Shutdown() override;

	Glitch* getRandomGlitch();
	Glitch* getRandomGlitch(int& index);
	std::vector<TimedGlitch*>* GetTimedGlitches() { return &timedGlitches; }
	FAssetRegistryModule& GetAssetRegistry() { return *assetRegistry; }

	ACharacter* getPlayer();
	
	static UFPSGameInstance* GetInstance() { return instance; }

private:
	FAssetRegistryModule* assetRegistry;
	std::vector<Glitch*> allGlitches;
	std::vector<TimedGlitch*> timedGlitches;
	std::vector<int> glitchLootTable;
	AGlitchTimer* glitchTimer;
	inline static UFPSGameInstance* instance = nullptr;
};
