// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FPSProjectile.h"
#include "ParticleDefinitions.h"

#include <with_fire_and_glitch/Public/HealthComponent.h>
#include "ExplosiveProjectile.generated.h"


/**
 * 
 */
class USphereComponent;
UCLASS()
class WITH_FIRE_AND_GLITCH_API AExplosiveProjectile : public AFPSProjectile
{
	GENERATED_BODY()
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		float Radius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		TSubclassOf<AFPSProjectile> shrapnel;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
			int numberOfBullets;
		/** Sound to play each time we fire */
		UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
			USoundBase* ExplosionSound;
		/** effect played on respawn */
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
			UParticleSystem* RespawnFX;
		FTimerHandle handle;
		FRotator FRandomRotator()
		{
			const float pitch = FMath::FRandRange(-180.f, 180.f);
			const float yaw = FMath::FRandRange(-180.f, 180.f);
			const float roll = FMath::FRandRange(-180.f, 180.f);
			return FRotator(pitch, yaw, roll);
		};
	AExplosiveProjectile();
	virtual void BeginPlay() override;

	void OnDetonate();

		void OnBeginOverlap(AActor* OverlapedActor, AActor* OtherActor) override;

	/** called when projectile hits something */

		void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

};
