// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BaseEnemyController.generated.h"

/**
 * 
 */
UCLASS()
class WITH_FIRE_AND_GLITCH_API ABaseEnemyController : public AAIController
{
	GENERATED_BODY()

	//DECLARE_EVENT(ABaseEnemyController, Attack)

public:
	ABaseEnemyController(const FObjectInitializer& ObjectInitializer);
};
