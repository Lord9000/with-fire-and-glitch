// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BaseAmmo.generated.h"

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	E_Pistol UMETA(DisplayName = "Pistol"),
	E_MachineGun UMETA(DisplayName = "MachineGun"),
	E_Grenade UMETA(DisplayName = "Grenade"),

};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WITH_FIRE_AND_GLITCH_API UBaseAmmo : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBaseAmmo();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		int ammoAmount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		EWeaponType ammoType;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
