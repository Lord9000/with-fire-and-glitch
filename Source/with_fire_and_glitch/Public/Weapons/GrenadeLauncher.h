// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/BasicGun.h"
#include "GrenadeLauncher.generated.h"

/**
 *
 */
UCLASS()
class WITH_FIRE_AND_GLITCH_API AGrenadeLauncher : public ABasicGun
{
	GENERATED_BODY()
public:
	AGrenadeLauncher();
	virtual void OnFire() override;

};
