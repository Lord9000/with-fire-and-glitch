// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasicGun.h"
#include "Katarynka.generated.h"

/**
 * 
 */
UCLASS()
class WITH_FIRE_AND_GLITCH_API AKatarynka : public ABasicGun
{
	GENERATED_BODY()
public:
	AKatarynka();


	virtual void OnFire() override;
	virtual void OnStopFire() override;
};
