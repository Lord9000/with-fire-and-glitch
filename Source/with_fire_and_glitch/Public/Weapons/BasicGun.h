// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <with_fire_and_glitch/Public/weapons/BaseAmmo.h>
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasicGun.generated.h"

UCLASS()
class WITH_FIRE_AND_GLITCH_API ABasicGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasicGun();


	/** Has the weapon been obtained by a player */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool isObtained;
	/** Intended index of the wepon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int index;

	FTimerHandle fireTimerHandle;
	/** Speed of shooting */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		float fireRate;

	/**Type of the weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		EWeaponType weaponType;

	/**Name of the weapon*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		FString name;
	/**max counter before overload*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	int maxCounter = 25;
	/**Is weapon overloaded*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	bool overload = false;
	/** flag if next projectile is glitched*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Glitch")
		bool nextProjectileGlitched;

	/** Counter to glitched projectile */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Glitch")
		int fireCounter;

	/** how many fires is need to glitch a weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Glitch")
		int toGlitchCounter;

	/** Method called when weapon fires*/
	virtual void OnFire() { fireCounter++; if (fireCounter > maxCounter)
	{
		overload = true;
		GetWorld()->GetTimerManager().SetTimer(fireTimerHandle, this, &ABasicGun::deOverload, 5.f, false);
	}
	}
	virtual void deOverload();
	/** Method called when weapon stops fire */
	virtual void OnStopFire() {
		
	}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
