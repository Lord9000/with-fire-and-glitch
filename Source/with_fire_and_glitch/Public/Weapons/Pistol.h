// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasicGun.h"
#include "Pistol.generated.h"

/**
 * 
 */
UCLASS()
class WITH_FIRE_AND_GLITCH_API APistol : public ABasicGun
{
	
	GENERATED_BODY()
public:
	APistol();
	virtual void OnFire() override;
};
