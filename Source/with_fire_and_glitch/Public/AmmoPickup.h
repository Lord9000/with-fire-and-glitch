// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BasePickup.h"
#include "Weapons/BaseAmmo.h"
#include "AmmoPickup.generated.h"

/**
 * 
 */
UCLASS()
class WITH_FIRE_AND_GLITCH_API AAmmoPickup : public ABasePickup
{
	GENERATED_BODY()	

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		int ammoAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		EWeaponType type;

	virtual void PickupAction(AFPSCharacter* character) override;
};
