// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.h"

#include "BaseEnemyAttack.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WITH_FIRE_AND_GLITCH_API UBaseEnemyAttack : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBaseEnemyAttack();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float attackInterval;

	UFUNCTION(BlueprintCallable)
	virtual bool CanAttack(UHealthComponent* target);

	UFUNCTION(BlueprintCallable)
	virtual bool Attack(UHealthComponent* target);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float attackTimer;

	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
