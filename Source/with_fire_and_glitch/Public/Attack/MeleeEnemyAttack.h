// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Attack/BaseEnemyAttack.h"
#include "MeleeEnemyAttack.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WITH_FIRE_AND_GLITCH_API UMeleeEnemyAttack : public UBaseEnemyAttack
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float meleeRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float meleeDamage;

	virtual bool CanAttack(UHealthComponent* target) override;
	virtual bool Attack(UHealthComponent* target) override;
	
};
