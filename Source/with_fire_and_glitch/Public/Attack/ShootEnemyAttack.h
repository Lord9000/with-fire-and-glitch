// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Attack/BaseEnemyAttack.h"
#include "FPSProjectile.h"
#include "ShootEnemyAttack.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WITH_FIRE_AND_GLITCH_API UShootEnemyAttack : public UBaseEnemyAttack
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AFPSProjectile> projectileClass;

	virtual bool CanAttack(UHealthComponent* target) override;
	virtual bool Attack(UHealthComponent* target) override;
};
