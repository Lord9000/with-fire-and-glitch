// Fill out your copyright notice in the Description page of Project Settings.
#include "AmmoPickup.h"
#include "FPSCharacter.h"
#include "Weapons/BaseAmmo.h"

void AAmmoPickup::PickupAction(AFPSCharacter* character) 
{
	if (character)
	{
		switch (this->type)
		{
		case EWeaponType::E_Pistol:
			character->pistolAmmo += this->ammoAmount;
			break;

		case EWeaponType::E_MachineGun:
			character->machineGunAmmo += this->ammoAmount;
			break;

		case EWeaponType::E_Grenade:
			character->grenadeAmmo += this->ammoAmount;
			break;
		}

		Destroy();
	}
}