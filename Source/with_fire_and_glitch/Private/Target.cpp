// Fill out your copyright notice in the Description page of Project Settings.

#include "Target.h"

// Sets default values
ATarget::ATarget()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Health = 100.f;
	TargetMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Pickup Mesh"));
	

}

void ATarget::DestroyTarget()
{
	Destroy();
}

// Called when the game starts or when spawned
void ATarget::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATarget::DamageTarget(float Damage)
{
	Health -= Damage;
	{
		if (Health <= 0.f)
		{
			DestroyTarget();
		}
	}
}

// Called every frame
void ATarget::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

