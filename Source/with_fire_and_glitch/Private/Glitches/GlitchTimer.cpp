// Fill out your copyright notice in the Description page of Project Settings.


#include "Glitches/GlitchTimer.h"
#include "Glitches/TimedGlitch.h"
#include "Engine/World.h"
#include "FPSGameInstance.h"

// Sets default values
AGlitchTimer::AGlitchTimer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGlitchTimer::BeginPlay()
{
	Super::BeginPlay();
	glitches = Cast<UFPSGameInstance>(GetWorld()->GetGameInstance())->GetTimedGlitches();
}

// Called every frame
void AGlitchTimer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	std::vector<GlitchedObject>* glitched = nullptr;
	for (int i = 0; i < glitches->size(); ++i)
	{
		glitched = (*glitches)[i]->GetGlitchedObjects();
		for (int j = 0; j < glitched->size(); ++j)
		{
			(*glitches)[i]->OnGlitchTick((*glitched)[j].comp);
			(*glitched)[j].time -= DeltaTime;
			if ((*glitched)[j].time <= 0.0001f)
			{
				(*glitches)[i]->DiscardGlitch((*glitched)[j].comp);
				if (GEngine)
					GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Discard Glitch"));
			}
		}
	}

}

