// Fill out your copyright notice in the Description page of Project Settings.


#include "Glitches/ColliderGlitch.h"
#include "Templates/Casts.h"
#include "Components/MeshComponent.h"

ColliderGlitch::ColliderGlitch()
{
}

ColliderGlitch::~ColliderGlitch()
{
}

void ColliderGlitch::ApplyGlitch(UActorComponent* comp)
{
	auto mesh = Cast<UPrimitiveComponent>(comp);
	if (mesh != nullptr)
	{
		mesh->GetOwner()->ConditionalBeginDestroy();
	}
}