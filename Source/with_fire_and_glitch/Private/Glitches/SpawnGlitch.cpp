// Fill out your copyright notice in the Description page of Project Settings.


#include "Glitches/SpawnGlitch.h"

SpawnGlitch::SpawnGlitch()
{
	spawnActorClass = AActor::StaticClass();
}

SpawnGlitch::~SpawnGlitch()
{
	spawnActorClass = AActor::StaticClass();
}

void SpawnGlitch::ApplyGlitch(UActorComponent* hitComp, UActorComponent* proComponent)
{
	if (spawnActorClass != AActor::StaticClass())
	{
		auto projectile = Cast<UPrimitiveComponent>(proComponent);
		auto proTransform = projectile->GetComponentTransform();
		FActorSpawnParameters spawnParams;
		proComponent->GetWorld()->SpawnActor<AActor>(spawnActorClass, proTransform.GetLocation(), FRotator::ZeroRotator, spawnParams);
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Red, TEXT("SPAWN ACTOR ERROR"));
	}
}