// Fill out your copyright notice in the Description page of Project Settings.


#include "Glitches/TeleportGlitch.h"
#include "GameFramework/Character.h"
#include "FPSGameInstance.h"
#include "Kismet/GameplayStatics.h"

TeleportGlitch::TeleportGlitch()
{
	teleport = false;
}

TeleportGlitch::~TeleportGlitch()
{

}

void TeleportGlitch::OnGlitchTick(UActorComponent* comp)
{
	if (teleport)
	{
		auto player = UFPSGameInstance::GetInstance()->getPlayer();
		auto oldTransform = player->GetTransform();
		auto normalized = (teleportMarker - oldTransform.GetLocation()).GetSafeNormal();
		float force = 30 * tickDistance;///*100.0f **/ (tickDistance / (GTIMERTICK * GTIMERTICK));
		player->LaunchCharacter(force * normalized, false, true);
		/*
		oldTransform.SetLocation(oldTransform.GetLocation() + normalized * tickDistance + FVector(0, 0, 5));

		player->SetActorTransform(oldTransform, false, nullptr);*/
	}

}

void TeleportGlitch::ApplyGlitch(UActorComponent* hitComp, UActorComponent* proComponent)
{
	if (teleport && objects.size() > 0)
	{
		objects[0].time = duration;
	}
	else
	{
		TimedGlitch::ApplyGlitch(nullptr, proComponent);
	}
	auto projectile = Cast<UPrimitiveComponent>(proComponent);
	auto proTransform = projectile->GetComponentTransform();
	auto player = UFPSGameInstance::GetInstance()->getPlayer();
	if (player != nullptr)
	{
		auto oldTransform = player->GetTransform();
		auto distance = FVector::Dist(oldTransform.GetLocation(), proTransform.GetLocation());
		tickDistance = distance * GTIMERTICK;
		teleportMarker = proTransform.GetLocation();
		//GEngine->AddOnScreenDebugMessage(-1, 15, FColor::Red, teleportMarker.ToCompactString());
		teleport = true;
		//player->LaunchCharacter((proTransform.GetLocation() - oldTransform.GetLocation()) * 10, true, true);
		//oldTransform.SetLocation(proTransform.GetLocation());
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 15, FColor::Red, TEXT("PLAYER IS NULL"));
	}
}
void TeleportGlitch::DiscardGlitch(UActorComponent* comp)
{
	auto player = UFPSGameInstance::GetInstance()->getPlayer();
	auto oldTransform = player->GetTransform();
	/*if (FVector::Dist(oldTransform.GetLocation(), teleportMarker) > 0.1f)
	{
		oldTransform.SetLocation(teleportMarker);
		player->SetActorTransform(oldTransform, false, nullptr);
	}*/
	teleportMarker = FVector::ZeroVector;
	teleport = false;
	TimedGlitch::DiscardGlitch(comp);
}
