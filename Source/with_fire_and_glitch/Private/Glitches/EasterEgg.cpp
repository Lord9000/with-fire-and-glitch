// Fill out your copyright notice in the Description page of Project Settings.

#include "Glitches/EasterEgg.h"
#include "FPSGameInstance.h"

// Sets default values
AEasterEgg::AEasterEgg()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Egg"));
	mesh->SetSimulatePhysics(true);
}

// Called when the game starts or when spawned
void AEasterEgg::BeginPlay()
{
	TArray<FAssetData> data;
	FARFilter filter;
	filter.PackagePaths.Add("/Game/Placeholders");
	Cast<UFPSGameInstance>(GetGameInstance())->GetAssetRegistry().Get().GetAssets(filter, data);

	for (int i = 0; i < data.Num(); ++i)
	{
		if (data[i].AssetName.ToString().Contains("Egg"))
		{
			mesh->SetStaticMesh(Cast<UStaticMesh>(data[i].GetAsset()));
			break;
		}
	}
	Super::BeginPlay();
	
}

// Called every frame
void AEasterEgg::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

