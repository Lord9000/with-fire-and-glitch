// Fill out your copyright notice in the Description page of Project Settings.


#include "Glitches/TimedGlitch.h"
#include <algorithm>

TimedGlitch::TimedGlitch()
{
	duration = 10;
}

TimedGlitch::TimedGlitch(float duration)
{
	this->duration = duration;
}

TimedGlitch::~TimedGlitch()
{
	duration = 0;
}

void TimedGlitch::ApplyGlitch(UActorComponent* comp)
{
	objects.push_back(GlitchedObject(comp, duration));
}

void TimedGlitch::ApplyGlitch(UActorComponent* hitComp, UActorComponent* proComponent)
{
	objects.push_back(GlitchedObject(hitComp, duration));
}

void TimedGlitch::DiscardGlitch(UActorComponent* comp)
{
	for (int i = 0; i < objects.size(); ++i)
	{
		if (objects[i].comp == comp)
		{
			objects.erase(objects.begin() + i);
			break;
		}
	}
}