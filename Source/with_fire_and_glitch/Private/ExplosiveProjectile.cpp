// Fill out your copyright notice in the Description page of Project Settings.


#include "ExplosiveProjectile.h"
#include "Components/SphereComponent.h"

#include "GameFramework/ProjectileMovementComponent.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>


AExplosiveProjectile::AExplosiveProjectile() : AFPSProjectile()
{

	Radius = 500;
	this->damage = 100;

	// Set as root component
	RootComponent = CollisionComp;
	this->ProjectileMovement->ProjectileGravityScale = 1;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

void AExplosiveProjectile::BeginPlay()
{
	Super::BeginPlay();

	
	GetWorld()->GetTimerManager().SetTimer(handle, this, &AExplosiveProjectile::OnDetonate, 2.f, false);
}



void AExplosiveProjectile::OnDetonate()
{
	TArray<FHitResult> HitActors;

	FVector StartTrace = GetActorLocation();
	FVector EndTrace = StartTrace;
	EndTrace.Z += 300.f;
	
	FCollisionShape ColllisionShape;
	ColllisionShape.ShapeType = ECollisionShape::Sphere;
	ColllisionShape.SetSphere(Radius);
	// Grabs location from the mesh that must have a socket called "Muzzle" in his skeleton
	FVector MuzzleLocation = GetActorLocation();

	if (RespawnFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, RespawnFX, GetActorLocation(), GetActorRotation());
	}
	if (ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ExplosionSound, GetActorLocation());
	}
	//Set Spawn Collision Handling Override
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
	for (int i = 0; i < numberOfBullets; i++)
	{
		 
		auto result = FMath::VRand();
		result *= FMath::RandRange(1, 20); 
		// spawn the projectile at the muzzle
		auto* projectile1 = GetWorld()->SpawnActor<AFPSProjectile>(shrapnel, (MuzzleLocation + result), FRandomRotator(), ActorSpawnParams);
		if (projectileGlitch != nullptr)
		{
			projectile1->projectileGlitch = projectileGlitch;
		}
		//CollisionComp->AddImpulseAtLocation(GetVelocity() * 1000.0f, GetActorLocation());
	}


	if (GetWorld()->SweepMultiByChannel(HitActors, StartTrace, EndTrace, FQuat::FQuat(), ECC_Pawn, ColllisionShape))
	{
		for (auto Actors = HitActors.CreateIterator(); Actors; Actors++)
		{
			auto* healthActor = (*Actors).GetActor();
			if (healthActor)
			{
				auto* healthCp = Cast<UHealthComponent>(healthActor->GetComponentByClass(UHealthComponent::StaticClass()));
				if (healthCp)
				{
					healthCp->Damage(damage);
				}
			}	

		}

	}
	
	Destroy();
}



void AExplosiveProjectile::OnBeginOverlap(AActor* OverlapedActor, AActor* OtherActor)
{
	
	// shooter may be nullptr sometimes, because SpawnActor sometimes returns nullptr BUT this function is called anyway - idk why
	if (OtherActor != shooter && shooter != nullptr)
	{

	
			OnDetonate();
		
		

	}

}

void AExplosiveProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("I just started running"));
	if ((OtherActor != NULL) && (OtherActor != this) && (HitComp != NULL))
	{
		
		OnDetonate();
		
	}
	

	UE_LOG(LogTemp, Warning, TEXT("I just starteaqsdfqwd running"));
	Destroy();
}
