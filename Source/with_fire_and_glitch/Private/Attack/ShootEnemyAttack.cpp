// Fill out your copyright notice in the Description page of Project Settings.


#include "Attack/ShootEnemyAttack.h"



bool UShootEnemyAttack::CanAttack(UHealthComponent* target)
{
	return Super::CanAttack(target);
}

bool UShootEnemyAttack::Attack(UHealthComponent* target)
{

	bool can = CanAttack(target);

	if (can)
	{
		auto* owner = GetOwner();

		//Set Spawn Collision Handling Override
		FActorSpawnParameters ActorSpawnParams;
		ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

		auto* projectile = GetWorld()->SpawnActor<AFPSProjectile>(
			projectileClass,
			owner->GetActorLocation(),
			owner->GetActorRotation(),
			ActorSpawnParams
		);

		if (projectile)
		{
			projectile->shooter = owner;
		}

		attackTimer = attackInterval;
	}

	return can;
}