// Fill out your copyright notice in the Description page of Project Settings.


#include "Attack/MeleeEnemyAttack.h"

bool UMeleeEnemyAttack::CanAttack(UHealthComponent* target)
{
	if (!Super::CanAttack(target))
		return false;

	auto sqDist = FVector::DistSquared(this->GetOwner()->GetActorLocation(), target->GetOwner()->GetActorLocation());

	if (sqDist > meleeRange * meleeRange)
		return false;

	return true;
}

bool UMeleeEnemyAttack::Attack(UHealthComponent* target)
{
	bool can = CanAttack(target);

	if (can)
	{
		target->Damage(meleeDamage);

		attackTimer = attackInterval;
	}

	return can;
}