// Fill out your copyright notice in the Description page of Project Settings.


#include "Attack/BaseEnemyAttack.h"

// Sets default values for this component's properties
UBaseEnemyAttack::UBaseEnemyAttack()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBaseEnemyAttack::BeginPlay()
{
	Super::BeginPlay();

	attackTimer = attackInterval;
	// ...
	
}


// Called every frame
void UBaseEnemyAttack::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	attackTimer -= DeltaTime;
	// ...
}


bool UBaseEnemyAttack::CanAttack(UHealthComponent* target)
{
	if (target == nullptr)
		return false;

	if (attackTimer > 0)
		return false;

	return true;
}


bool UBaseEnemyAttack::Attack(UHealthComponent* target)
{
	return false;
}
