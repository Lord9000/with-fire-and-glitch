// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSGameInstance.h"
#include "Engine/World.h"
#include "Glitches/GlitchTimer.h"
#include "Glitches/ColliderGlitch.h"
#include "Glitches/SpawnGlitch.h"
#include "Glitches/TeleportGlitch.h"
#include "Glitches/EasterEgg.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"

void UFPSGameInstance::Init()
{
	instance = this;
	assetRegistry = &FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");

	allGlitches.push_back(new ColliderGlitch());
	allGlitches.push_back(new SpawnGlitch());
	((SpawnGlitch*)allGlitches[1])->SetSpawnActor(AEasterEgg::StaticClass());
	allGlitches.push_back(new TeleportGlitch(1.0f));
	timedGlitches.push_back((TeleportGlitch*)allGlitches[2]);

	glitchTimer = Cast<AGlitchTimer>(GetWorld()->SpawnActor(AGlitchTimer::StaticClass()));
	glitchTimer->SetActorTickInterval(GTIMERTICK);

	for (int i = 0; i < 6; ++i)
	{
		glitchLootTable.push_back(0);
	}
	for (int i = 0; i < 5; ++i)
	{
		glitchLootTable.push_back(1);
	}
	for (int i = 0; i < 2; ++i)
	{
		glitchLootTable.push_back(2);
	}
}

void UFPSGameInstance::Shutdown()
{
	for (int i = 0; i < allGlitches.size(); ++i)
	{
		delete allGlitches[i];
		allGlitches[i] = nullptr;
	}
	allGlitches.clear();
	timedGlitches.clear();
}

ACharacter* UFPSGameInstance::getPlayer()
{
	
	UWorld* world = GetWorld();
	if (world == nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15, FColor::Red, TEXT("THERE IS NO WORLD XD"));
	}
	ACharacter* player = UGameplayStatics::GetPlayerCharacter(world, 0);
	return player;
}

Glitch* UFPSGameInstance::getRandomGlitch()
{
	if (allGlitches.size() > 1)
	{
		return allGlitches[glitchLootTable[FMath::RandRange(0, glitchLootTable.size() - 1)]];
	}
	else
	{
		return allGlitches[0];
	}
}

Glitch* UFPSGameInstance::getRandomGlitch(int& index)
{
	if (allGlitches.size() > 1)
	{
		index = FMath::RandRange(0, allGlitches.size() - 1);
		return allGlitches[index];
	}
	else
	{
		return allGlitches[0];
	}
}
