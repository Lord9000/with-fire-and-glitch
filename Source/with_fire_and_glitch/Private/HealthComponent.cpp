// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthComponent.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>
#include "FPSCharacter.h"
#include "FPSGameInstance.h"
// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


void UHealthComponent::Damage(int hp)
{
	if (DamageFX && GetOwner() != UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, DamageFX, GetOwner()->GetActorLocation(), GetOwner()->GetActorRotation());
	}
	if ( GetOwner() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))
	{

			if (auto* player = Cast<AFPSCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
				player->DamagePostprocessInvoke();
	}

	currentHealth -= hp;

	if (currentHealth <= 0)
		GetOwner()->Destroy();
}


void UHealthComponent::Heal(int hp)
{
	currentHealth += hp;
	if ( GetOwner() == UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))
	{

		if (auto* player = Cast<AFPSCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)))
			player->HealPostprocessInvoke();

	}
	if (currentHealth > maxHealth)
		currentHealth = maxHealth;
}

int UHealthComponent::GetMaxHealth()
{
	return maxHealth;
}

int UHealthComponent::GetCurrentHealth()
{
	return currentHealth;
}
