// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/Tasks/ClearKey.h"
#include "BehaviorTree/BlackboardComponent.h"

UClearKey::UClearKey(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NodeName = "Clear Key";

	// empty KeySelector = allow everything
}

EBTNodeResult::Type UClearKey::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	OwnerComp.GetBlackboardComponent()->ClearValue(BlackboardKey.GetSelectedKeyID());

	return EBTNodeResult::Succeeded;
}