// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/BaseEnemyController.h"
#include <with_fire_and_glitch/Public/Attack/BaseEnemyAttack.h>

ABaseEnemyController::ABaseEnemyController(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
	SetGenericTeamId(FGenericTeamId(13));
}