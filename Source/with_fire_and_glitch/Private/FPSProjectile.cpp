// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSProjectile.h"
#include "HealthComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Glitches/Glitch.h"

AFPSProjectile::AFPSProjectile()
	: shooter(nullptr)
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AFPSProjectile::OnHit);	// set up a notification for when this component hits something blocking
	
	this->OnActorBeginOverlap.AddDynamic(this, &AFPSProjectile::OnBeginOverlap);

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 6000.f;
	ProjectileMovement->MaxSpeed = 6000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;
	ProjectileMovement->ProjectileGravityScale = 0;
	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	projectileGlitch = nullptr;
}


void AFPSProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		//OtherComp->AddImpulseAtLocation(GetVelocity() * 1000.0f, GetActorLocation());
		if (projectileGlitch != nullptr)
		{
			if (GEngine)
				GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Green, TEXT("Apply Glitch"));
			//one or other func will be called
			projectileGlitch->ApplyGlitch(OtherComp);
			projectileGlitch->ApplyGlitch(OtherComp, HitComp);
		}
	}
	Destroy();
}

void AFPSProjectile::OnBeginOverlap(AActor* OverlapedActor, AActor* OtherActor)
{
	// shooter may be nullptr sometimes, because SpawnActor sometimes returns nullptr BUT this function is called anyway - idk why
	if (OtherActor != shooter && shooter != nullptr)
	{
		if (auto* healthCp = (UHealthComponent*)OtherActor->GetComponentByClass(UHealthComponent::StaticClass()))
			healthCp->Damage(damage);
	}
}
