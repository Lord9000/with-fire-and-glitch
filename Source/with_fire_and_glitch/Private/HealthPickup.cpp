// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthPickup.h"

#include "HealthComponent.h"
#include "FPSCharacter.h"

void AHealthPickup::PickupAction(AFPSCharacter* character)
{
	if (character)
	{
		UHealthComponent* healthComponent = character->FindComponentByClass<UHealthComponent>();
		if (healthComponent != nullptr)
		{
			if (healthComponent->GetCurrentHealth() < healthComponent->GetMaxHealth())
			{
				healthComponent->Heal(this->healAmount);
				Destroy();
			}			
		}
	}
}