// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePickup.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "FPSCharacter.h"
#include "Weapons/BasicGun.h"
#include "..\Public\BasePickup.h"

// Sets default values
ABasePickup::ABasePickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->RootComponent = CreateDefaultSubobject<USceneComponent >("Root");

	meshRoot = CreateDefaultSubobject<USceneComponent >("MeshRoot");
	meshRoot->AttachTo(RootComponent);

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->CastShadow = true;
	BaseMesh->SetSimulatePhysics(false);
	BaseMesh->AttachTo(meshRoot);

	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(50.0f);
	CollisionComp->SetCollisionProfileName("Pickup");

	CollisionComp->AttachTo(RootComponent);

	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &ABasePickup::OnOverlapBegin);
}

void ABasePickup::OnOverlapBegin(UPrimitiveComponent* OveralappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		if (OtherActor->IsA(AFPSCharacter::StaticClass()))
		{
			PickupAction(Cast<AFPSCharacter>(OtherActor));
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("BEGIN OVERLAP"));
		}			
	}
}

void ABasePickup::OnOverlapEnd(UPrimitiveComponent* OveralappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		if (OtherActor->IsA(AFPSCharacter::StaticClass()))
		{

		}		
	}	
}

void ABasePickup::PickupAction(AFPSCharacter* character)
{

}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay()
{
	Super::BeginPlay();
	CollisionComp->OnComponentEndOverlap.AddDynamic(this, &ABasePickup::OnOverlapEnd);
}

// Called every frame
void ABasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);			
}

