// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSCharacter.h"
#include "FPSProjectile.h"
#include "FPSGameInstance.h"
#include "Glitches/TimedGlitch.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimSequence.h"
#include <Runtime/Engine/Public/DrawDebugHelpers.h>
#include <with_fire_and_glitch/Public/Target.h>



AFPSCharacter::AFPSCharacter()
{
	// Create a CameraComponent	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraComponent->SetupAttachment(GetCapsuleComponent());
	CameraComponent->SetRelativeLocation(FVector(0, 0, BaseEyeHeight)); // Position the camera
	CameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1PComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh"));
	Mesh1PComponent->SetupAttachment(CameraComponent);
	Mesh1PComponent->CastShadow = false;
	Mesh1PComponent->SetRelativeRotation(FRotator(2.0f, -15.0f, 5.0f));
	Mesh1PComponent->SetRelativeLocation(FVector(0, 0, -160.0f));

	// Create a gun mesh component
	GunMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	GunMeshComponent->CastShadow = false;

	GunMeshComponent->SetupAttachment(Mesh1PComponent, "FPGun");

	PawnNoiseEmitter = CreateDefaultSubobject<UPawnNoiseEmitterComponent>(TEXT("PawnNoiseEmitterComponent"));
	
	teamID = FGenericTeamId(21);

	grenadeAmmo = 2;
	pistolAmmo = 12;
	machineGunAmmo = 120;
	gunIndex = 0;
	isShooting = false;
	isZoomedIn = false;
	 outofAmmo = false;
	jumping = false;
	isGlitched = false;
	jumpCount = 0;
}

void AFPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (jumping)
	{
		Jump();
	}
	

}

void AFPSCharacter::BeginPlay()
{
	APistol* pistol = GetWorld()->SpawnActor<APistol>();
	pistol->toGlitchCounter = 5;
	GunsCollection.Add(pistol);
	AKatarynka* katarynka = GetWorld()->SpawnActor<AKatarynka>();
	GunsCollection.Add(katarynka);
	katarynka->toGlitchCounter = 10;
	AGrenadeLauncher* grenadeLauncher = GetWorld()->SpawnActor<AGrenadeLauncher>();
	GunsCollection.Add(grenadeLauncher);
	grenadeLauncher->toGlitchCounter = 2;
	Super::BeginPlay();

	//Get Assets in C++ style
	
	TArray<FAssetData> data;
	FARFilter filter;
	filter.PackagePaths.Add("/Game/Placeholders/Skeletals");
	Cast<UFPSGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()))->GetAssetRegistry().Get().GetAssets(filter, data);

	for (int i = 0; i < data.Num(); ++i)
	{
		for (int j = 0; j < data.Num() - 1; ++j)
		{
			if (data[j + 1].AssetName.ToString()[0] < data[j].AssetName.ToString()[0])
			{
				data.Swap(j, j + 1);
			}
		}
	}

	for (int i = 0; i < data.Num(); ++i)
	{
		GunMeshes.Add(Cast<USkeletalMesh>(data[i].GetAsset()));
	}

	//Generate starting glitches
	int index = 0;
	for (int i = 0; i < MAX_GLITCHES; ++i)
	{
		nextGlitchesList[i] = Cast<UFPSGameInstance>(GetWorld()->GetGameInstance())->getRandomGlitch(index);
		UnrealNextGlitchesList.Add(static_cast<EGlitchType>(index));
	}
	glitchIterator = 0;

	SwitchWeaponMesh(0);
}

void AFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &AFPSCharacter::ZoomIn);
	PlayerInputComponent->BindAction("Zoom", IE_Released, this, &AFPSCharacter::StopZoom);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFPSCharacter::StartFirirng);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AFPSCharacter::StopFirirng);
	PlayerInputComponent->BindAction("SwitchPrimaryNext", IE_Pressed, this, &AFPSCharacter::SwitchToNextPrimaryWeapon);
	PlayerInputComponent->BindAction("SwitchPrimaryPre", IE_Pressed, this, &AFPSCharacter::SwitchToPreviousPrimaryWeapon);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFPSCharacter::CheckJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AFPSCharacter::CheckJump);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFPSCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFPSCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}




void AFPSCharacter::AddAmmo(EWeaponType _weaponType, int _ammoAmount)
{
	switch (_weaponType)
	{
	case EWeaponType::E_Pistol:
		pistolAmmo += _ammoAmount;
		break;
	case EWeaponType::E_MachineGun:
		machineGunAmmo += _ammoAmount;
		break;
	case EWeaponType::E_Grenade:
		grenadeAmmo += _ammoAmount;
		break;
	}
}
void AFPSCharacter::SwitchToNextPrimaryWeapon()
{
	outofAmmo = false;
	gunIndex = (gunIndex + 1) % GunsCollection.Num();
	
	SwitchWeaponMesh(gunIndex);
	OnWeaponChange();
	StopFirirngWithoutMeshUpdate();
}

void AFPSCharacter::SwitchToPreviousPrimaryWeapon()
{
	gunIndex = (GunsCollection.Num() + gunIndex - 1) % GunsCollection.Num();
	
	SwitchWeaponMesh(gunIndex);
	OnWeaponChange();
	StopFirirngWithoutMeshUpdate();
}

void AFPSCharacter::StartFirirng()
{
	isShooting = true;
	Fire();
	GunsCollection[gunIndex]->fireTimerHandle.Invalidate();
}

void AFPSCharacter::StopFirirngWithoutMeshUpdate()
{
	isShooting = false;
	GunsCollection[gunIndex]->OnStopFire();
	if(!GunsCollection[gunIndex]->nextProjectileGlitched)DeActivateGlitchPostprocess();
	fireTimerHandle.Invalidate();
	int index = 0;
	nextGlitchesList[glitchIterator] = Cast<UFPSGameInstance>(GetWorld()->GetGameInstance())->getRandomGlitch(index);
	UnrealNextGlitchesList[glitchIterator] = static_cast<EGlitchType>(index);
	glitchIterator = ++glitchIterator % MAX_GLITCHES;
}

void AFPSCharacter::StartFirirngAfterOverload()
{
	GunsCollection[gunIndex]->OnStopFire();
	isShooting = true;
	Fire();
}

void AFPSCharacter::StopFirirng()
{
	StopFirirngWithoutMeshUpdate();
	ShotsFired();
}

void AFPSCharacter::FireaRaycast()
{
	FHitResult* HitResult = new FHitResult();
	FVector StartTrace = CameraComponent->GetComponentLocation();
	FVector ForwardVector = CameraComponent->GetForwardVector();
	FVector EndTrace = ((ForwardVector *5000.f)+ StartTrace);
	FCollisionQueryParams* TraceParams = new FCollisionQueryParams();

	if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartTrace, EndTrace, ECC_Visibility, *TraceParams))
	{
		DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor(255, 0, 0),false, 5.f);
		ATarget* TestTarget = Cast< ATarget>(HitResult->Actor.Get());
		if (TestTarget != NULL && !TestTarget->IsPendingKill())
		{
			TestTarget->DamageTarget(50.f);
		}
	}
}

void AFPSCharacter::Fire()
{
	if (isShooting )
	{
		if (GunsCollection[gunIndex]!=nullptr)
		{
			if (!GunsCollection[gunIndex]->overload)
			{
				switch (GunsCollection[gunIndex]->weaponType)
				{
				case EWeaponType::E_Pistol:
				{
					if (pistolAmmo == 0)
						outofAmmo = true;
					else
					{
						outofAmmo = false;
						pistolAmmo -= 1;
					}
					break;
				}

				case EWeaponType::E_MachineGun:
				{
					if (machineGunAmmo == 0)
						outofAmmo = true;
					else
					{
						outofAmmo = false;
						machineGunAmmo -= 1;
					}
					break;
				}
				case EWeaponType::E_Grenade:
				{
					if (grenadeAmmo == 0)
						outofAmmo = true;
					else
					{
						outofAmmo = false;
						grenadeAmmo -= 1;
					}
					break;
				}
				}
				if (outofAmmo == false)
				{
					if (SmokeFX)
					{
						FVector MuzzleLocation = GunMeshComponent->GetComponentLocation();
						MuzzleLocation.Y += 30;
						UGameplayStatics::SpawnEmitterAtLocation(this, SmokeFX, MuzzleLocation, GunMeshComponent->GetSocketRotation("Muzzle"));

					}
					// try and fire a projectile
					if (ProjectileClass)
					{
						// Grabs location from the mesh that must have a socket called "Muzzle" in his skeleton
						FVector MuzzleLocation = GunMeshComponent->GetSocketLocation("Muzzle");
						// Use controller rotation which is our view direction in first person
						FRotator MuzzleRotation = GetControlRotation();

						//Set Spawn Collision Handling Override
						FActorSpawnParameters ActorSpawnParams;
						ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

						// spawn the projectile at the muzzle
						auto* projectile = GetWorld()->SpawnActor<AFPSProjectile>(ProjectileClass, MuzzleLocation, MuzzleRotation, ActorSpawnParams);
						if (projectile)
						{
							projectile->shooter = this;
						}
						if (GunsCollection[gunIndex]->nextProjectileGlitched)
						{
							projectile->projectileGlitch = nextGlitchesList[glitchIterator];
						}
						GunsCollection[gunIndex]->OnFire();
						if (GunsCollection[gunIndex]->nextProjectileGlitched)ActivateGlitchPostprocess();
						else DeActivateGlitchPostprocess();
						ShotsFired();

						PawnNoiseEmitter->MakeNoise(this, shootNoise, MuzzleLocation);
					}

					// try and play the sound if specified
					if (FireSound)
					{
						UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
					}

					// try and play a firing animation if specified
					if (FireAnimation)
					{
						// Get the animation object for the arms mesh
						UAnimInstance* AnimInstance = Mesh1PComponent->GetAnimInstance();
						if (AnimInstance)
						{
							AnimInstance->PlaySlotAnimationAsDynamicMontage(FireAnimation, "Arms", 0.0f);
						}
					}

				
						}
			}
			else DeActivateGlitchPostprocess();
			GetWorld()->GetTimerManager().SetTimer(fireTimerHandle, this, &AFPSCharacter::Fire, GunsCollection[gunIndex]->fireRate, false);

				
			
		

		}
	}
}


void AFPSCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}


void AFPSCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}


void AFPSCharacter::ZoomIn()
{

if (auto firstPersonCamera = GetFirstPersonCameraComponent())
{
	firstPersonCamera->SetFieldOfView(60.f);
	isZoomedIn = true;
}
}

void AFPSCharacter::StopZoom()
{
	if (auto firstPersonCamera = GetFirstPersonCameraComponent())
	{
		firstPersonCamera->SetFieldOfView(90.f);
		isZoomedIn = false;
	}
}

void AFPSCharacter::CheckJump()
{
	if (jumping)
	{
		jumping = false;
	}
	else
	{
		jumping = true;
		jumpCount++;
		if (jumpCount == 2)
		{
			LaunchCharacter(FVector(0, 0, 500), false, true);
		}
	}
}

void AFPSCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);

	jumpCount = 0;
}

void AFPSCharacter::SetGenericTeamId(const FGenericTeamId& TeamID)
{
	this->teamID = TeamID;
}

FGenericTeamId AFPSCharacter::GetGenericTeamId() const
{
	return teamID;
}

void AFPSCharacter::HealPostprocessInvoke()
{
	HealPostprocess();
}

void AFPSCharacter::DamagePostprocessInvoke()
{
	DamagePostprocess();
}

int AFPSCharacter::GetAmmoByWeaponIndex(int index)
{
	switch (index)
	{
		case 0: return pistolAmmo;
		case 1: return machineGunAmmo;
		case 2: return grenadeAmmo;
	}
	return -1;
}

bool AFPSCharacter::IsNextBullteGlitched(int index)
{
	int ammo = index + GunsCollection[gunIndex]->fireCounter;
	int glitch = GunsCollection[gunIndex]->toGlitchCounter;

	if (gunIndex != 1)
		return !(ammo % glitch);
	else // Gatling
		return ammo > glitch;
}