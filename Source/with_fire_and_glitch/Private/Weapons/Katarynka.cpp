// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/Katarynka.h"

AKatarynka::AKatarynka()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isObtained = false;
	index = 0;

	weaponType = EWeaponType::E_MachineGun;
	fireRate = 0.1f;

	name = "Katarynka";
}

void AKatarynka::OnFire()
{
	Super::OnFire();
	
		if (fireCounter == toGlitchCounter)
		{
			nextProjectileGlitched = true;
		}
		

}

void AKatarynka::OnStopFire()
{
	Super::OnStopFire(); 
	if (!overload)
	{
		nextProjectileGlitched = false;
		fireCounter = 0;
	}
	
	
}