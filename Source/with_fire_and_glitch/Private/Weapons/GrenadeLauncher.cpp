// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/GrenadeLauncher.h"



AGrenadeLauncher::AGrenadeLauncher()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isObtained = false;
	index = 0;

	weaponType = EWeaponType::E_Grenade;
	fireRate = 2.5f;

	name = "Grenade";
}

void AGrenadeLauncher::OnFire()
{
	Super::OnFire();
	nextProjectileGlitched &= false;
	if (fireCounter == toGlitchCounter)
	{
		nextProjectileGlitched = true;
		fireCounter = 0;
	}
}
