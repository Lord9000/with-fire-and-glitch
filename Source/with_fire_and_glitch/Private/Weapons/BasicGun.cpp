// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/BasicGun.h"

// Sets default values
ABasicGun::ABasicGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isObtained = false;
	index = 0;

	weaponType = EWeaponType::E_Pistol;
	fireRate = 0.1f;
	
	name = "Weapon";
	fireCounter = 1;
	nextProjectileGlitched = false;
}

void ABasicGun::deOverload()
{
	overload = false;
	fireCounter = 0;
	nextProjectileGlitched = false;
}

// Called when the game starts or when spawned
void ABasicGun::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABasicGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

