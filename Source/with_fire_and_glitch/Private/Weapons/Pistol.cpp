// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapons/Pistol.h"

APistol::APistol()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	isObtained = false;
	index = 0;

	weaponType = EWeaponType::E_Pistol;
	fireRate = 1.5f;

	name = "Pistol";
}

void APistol::OnFire()
{
	Super::OnFire();
	nextProjectileGlitched &= false;
	if (fireCounter == toGlitchCounter)
	{
		nextProjectileGlitched = true;
		fireCounter = 0;
	}
}